﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PanasonicSevk
{
    public class Utilities
    {

        static OdbcConnection odbcConnection = null;
        static SqlConnection sqlConnection = null;

        static ConfigHelper config;
        public static ConfigHelper Config
        {
            get
            {
                if (config == null)
                {
                    config = ConfigHelper.LoadFile();
                }
                return config;
            }
        }

        public static OdbcConnection OdbcConnection
        {
            get
            {
                if (odbcConnection == null)
                {
                    try
                    {
                        odbcConnection = new OdbcConnection(string.Format(Config.UyumConnectionString, "{" + Config.UyumConnectionDriver + "}"));
                        odbcConnection.Open();
                    }
                    catch (Exception exc)
                    {
                        Msg("Sistemle bağlantı kurulurken hata oluştu:" + exc.Message);
                    }
                }
                if (odbcConnection.State != System.Data.ConnectionState.Open)
                    odbcConnection.Open();
                return odbcConnection;
            }
        }

        public static SqlConnection SqlConnection
        {
            get
            {
                if (sqlConnection == null)
                {
                    try
                    {
                        sqlConnection = new SqlConnection(string.Format(Config.SqlConnectionString, Config.DatabaseName));
                        sqlConnection.Open();
                    }
                    catch (Exception exc)
                    {
                        Msg("Sistemle bağlantı kurulurken hata oluştu:" + exc.Message);
                    }
                }
                if (sqlConnection.State != System.Data.ConnectionState.Open)
                    sqlConnection.Open();
                return sqlConnection;
            }
        }

        public static void CloseAll()
        {
            try
            {
                if (odbcConnection != null)
                {
                    odbcConnection.Close();
                    odbcConnection.Dispose();
                }
                if (sqlConnection != null)
                {
                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }
                sqlConnection = null;
                odbcConnection = null;
                System.Data.SqlClient.SqlConnection.ClearAllPools();
            }
            catch
            {
            }
        }


        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Trace.WriteLine(Config.SqlConnectionString);
            Trace.Write(OdbcConnection.State.ToString());
            Trace.Write(OdbcConnection.State.ToString());
            Application.Run(new FormMain());
        }

        public static void Msg(string msg)
        {
            Cursor.Current = Cursors.Default;
            MessageBox.Show(msg, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        public static void Inf(string msg)
        {
            Cursor.Current = Cursors.Default;
            MessageBox.Show(msg, "Dikkat", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
    }

    [Serializable]
    public class ConfigHelper
    {
        public ConfigHelper()
        {
        }

        private const string ConfigFile = "AppConfig.dat";

        public string UyumFirmaKod { get; set; } = "PESTR19";

        public string DatabaseName { get; set; } = "viko_2017";

        public string UyumConnectionDriver { get; set; } = "Progress OpenEdge 10.1B driver";
        public string UyumConnectionString { get; set; } = "Driver={0};HOST=10.48.10.132;PORT=1501;DB=viko2019;UID=sysprogress;PWD=Plstr@19;DefaultIsolationLevel=READ UNCOMMITTED;";
        public string SqlConnectionString { get; set; } = "packet size=4096;data source=10.48.10.139;persist security info=False;initial catalog={0};Connect Timeout=50;User=sa;Password=V:k0saP@ss2015;Pooling=False;";

        public static ConfigHelper LoadFile()
        {
            ConfigHelper config = null;
            byte[] arrBytes;

            if (File.Exists(Application.StartupPath + "\\" + ConfigFile))
            {
                using (FileStream stream = new FileStream(Application.StartupPath + "\\" + ConfigFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (BinaryReader reader = new BinaryReader(stream, Encoding.GetEncoding("windows-1254")))
                    {
                        arrBytes = reader.ReadBytes((int)reader.BaseStream.Length);
                        using (var memoryStream = new MemoryStream())
                        {
                            var binaryFormatter = new BinaryFormatter();
                            using (MemoryStream outputStream = new MemoryStream())
                            {
                                using (MemoryStream inputStream = new MemoryStream(arrBytes))
                                {
                                    using (GZipStream zip = new GZipStream(inputStream, CompressionMode.Decompress))
                                    {
                                        zip.CopyTo(outputStream);
                                    }
                                }
                                arrBytes = outputStream.ToArray();
                            }
                            memoryStream.Write(arrBytes, 0, arrBytes.Length);
                            memoryStream.Seek(0, SeekOrigin.Begin);

                            config = (ConfigHelper)binaryFormatter.Deserialize(memoryStream);
                        }
                    }
                }
            }
            else
            {
                config = new ConfigHelper();
                config.SaveFile();
            }
            return config;
        }

        public void SaveFile()
        {
            using (var memoryStream = new MemoryStream())
            {
                byte[] arrBytes;
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, this);
                arrBytes = memoryStream.ToArray();
                using (var outputStream = new MemoryStream())
                {
                    using (var zip = new GZipStream(outputStream, CompressionMode.Compress))
                    {
                        zip.Write(arrBytes, 0, arrBytes.Length);
                    }
                    arrBytes = outputStream.ToArray();
                }
                using (FileStream stream = new FileStream(Application.StartupPath + "\\" + ConfigFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write))
                {
                    using (BinaryWriter writer = new BinaryWriter(stream, Encoding.GetEncoding("windows-1254")))
                    {
                        writer.Write(arrBytes);
                        writer.Flush();
                    }
                }
            }
        }
    }
}
