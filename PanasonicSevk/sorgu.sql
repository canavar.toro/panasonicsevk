select  	e.semir_masterno as "SevkEmriId", e.emir_no as "SevkEmriNo", TO_CHAR(e.emir_tarih, 'yyyy-mm-dd') as "BelgeTarih", 	
cast(c.rowid as int) as "CariId", e.cari_kod as "CariKod", 	TO_CHAR(e.yukleme_tarih, 'yyyy-mm-dd') as "TeslimTarih", '' as "Aciklama", 	
c.cgrup_kod3 as "CariGrup", c.aktif_pasif as "CariDurum" 
from pub.sevk_emir e, pub.cari_kart c 
where e.firma_kod = 'PESTR19' and e.emir_durum = 1 and e.firma_kod = c.firma_kod and e.cari_kod = c.cari_kod 


select 	d.semir_detayno as "SevkEmriDetayId", e.semir_masterno as "SevkEmriId", d.emir_no as "SevkEmriNo",  	
d.sip_masterno as "SiparisId", d.stok_kod as "MalzemeKod", cast(s.rowid as int) as "MalzemeId", 	
cast(b.rowid as int) as "BirimId", s.birim as "Birim", cast(p.rowid as int) as "DepoId", 
p.depo_kod as "DepoKod",o.sip_no as "OzelKod", o.cari_kod as "CariKod", d.sira_no as "SiraNo",
d.ozellik_kod1 as "OzelKod1",d.sevk_miktar as "Miktar", d.dmiktar2 - d.teslim_miktar as "Kalan" 
from pub.sevk_emirdet d, pub.stok_kart s, pub.birim b, pub.siparis_detay o, pub.depo p, pub.sevk_emir e  
where d.firma_kod = 'PESTR19' and d.emir_durum = 1 and e.emir_durum = 1 and d.firma_kod = s.firma_kod and d.stok_kod = s.stok_kod 
and d.firma_kod = b.firma_kod and s.birim = b.birim and d.firma_kod = o.firma_kod and d.semir_masterno = e.semir_masterno and d.firma_kod = e.firma_kod
and d.sip_detayno = o.sip_detayno and d.sip_masterno = o.sip_masterno and d.firma_kod = p.firma_kod and o.depo_kod = p.depo_kod 


select cast(RowId as int) as "CariId",cari_kod as "CariKod",cari_ad as "CariAd",kisa_ad as "KisaAd",
bolge_kod as "BolgeKod",cgrup_kod3 as "GrupKod",adres1 as "Adres1",adres2 as "Adres2",adres3 as "Adres3",
fat_adres1 as "FaturaAdres1",fat_adres2 as "FaturaAdres2",fat_adres3 as "FaturaAdres3",
vergi_daire as "VargiDaire",vergi_no as "VergiNo",depo_kod as "DepoKod",cari_tip as "CariTip" 
from pub.cari_kart where firma_kod = 'PESTR19'