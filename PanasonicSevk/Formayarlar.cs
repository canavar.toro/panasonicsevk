﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PanasonicSevk
{
    public partial class Formayarlar : Form
    {
        public Formayarlar()
        {
            InitializeComponent();
        }

        private void Formayarlar_Load(object sender, EventArgs e)
        {
            txtfirmakod.Text = Utilities.Config.UyumFirmaKod;
            txtdatabase.Text = Utilities.Config.DatabaseName;
            txtdriver.Text = Utilities.Config.UyumConnectionDriver;
        }

        private void btnkaydet_Click(object sender, EventArgs e)
        {
            Utilities.Config.UyumConnectionDriver = txtdriver.Text;
            Utilities.Config.UyumFirmaKod = txtfirmakod.Text;
            Utilities.Config.DatabaseName = txtdatabase.Text;
            Utilities.Config.SaveFile();
            Utilities.Inf("Ayarlar kaydedildi, uygulamadan çıkıp tekrar girin.");
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnkapat_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
