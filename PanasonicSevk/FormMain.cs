﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PanasonicSevk
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void btnAyar_Click(object sender, EventArgs e)
        {
            Formayarlar ayar = new PanasonicSevk.Formayarlar();
            ayar.ShowDialog();
        }

        private void btnkapat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnDuzelt_Click(object sender, EventArgs e)
        {
            SqlTransaction transaction = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                SqlCommand sqlCommand = Utilities.SqlConnection.CreateCommand();
                transaction = Utilities.SqlConnection.BeginTransaction();
                sqlCommand.Transaction = transaction;

                OdbcCommand odbcCommand = Utilities.OdbcConnection.CreateCommand();
                odbcCommand.CommandText = "select  " +
    "	e.semir_masterno as \"SevkEmriId\", e.emir_no as \"SevkEmriNo\", TO_CHAR(e.emir_tarih, 'yyyy-mm-dd') as \"BelgeTarih\", " +
    "	cast(c.rowid as int) as \"CariId\", e.cari_kod as \"CariKod\", " +
    "	TO_CHAR(e.yukleme_tarih, 'yyyy-mm-dd') as \"TeslimTarih\", '' as \"Aciklama\", " +
    "	c.cgrup_kod3 as \"CariGrup\", c.aktif_pasif as \"CariDurum\" " +
    "from pub.sevk_emir e, pub.cari_kart c " +
    "where e.firma_kod = ? and e.emir_durum = 1 " +
    "and e.firma_kod = c.firma_kod and e.cari_kod = c.cari_kod and e.emir_no = ?";
                var param2 = new OdbcParameter("firma_kod", OdbcType.VarChar);
                param2.Value = Utilities.Config.UyumFirmaKod;
                odbcCommand.Parameters.Add(param2);
                var param1 = new OdbcParameter("emir_no", OdbcType.VarChar);
                param1.Value = txtsevkemrino.Text;
                odbcCommand.Parameters.Add(param1);

                OdbcDataAdapter adapter = new OdbcDataAdapter(odbcCommand);
                DataTable dtsevkemri = new DataTable();
                adapter.Fill(dtsevkemri);
                if (dtsevkemri != null && dtsevkemri.Rows.Count > 0)
                {
                    sqlCommand.CommandText = "DELETE FROM dbo.SevkEmriDetaylari WHERE SevkEmriId IN (SELECT SevkEmriId FROM dbo.SevkEmirleri WITH (NOLOCK) WHERE SevkEmriNo = @SevkEmriNo);DELETE dbo.SevkEmirleri WHERE SevkEmriNo = @SevkEmriNo;";
                    sqlCommand.Parameters.AddWithValue("SevkEmriNo", txtsevkemrino.Text);
                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.CommandText = "INSERT INTO dbo.SevkEmirleri (SevkEmriId,SevkEmriNo,BelgeTarih,CariId,CariKod,TeslimTarih,Aciklama,CariGrup, KaynakProgram) VALUES (@SevkEmriId,@SevkEmriNo,@BelgeTarih,@CariId,@CariKod,@TeslimTarih,@Aciklama,@CariGrup, @KaynakProgram)";
                    for (int i = 0; i < dtsevkemri.Rows.Count; i++)
                    {
                        sqlCommand.Parameters.Clear();
                        sqlCommand.Parameters.AddWithValue("SevkEmriId", dtsevkemri.Rows[i]["SevkEmriId"]);
                        sqlCommand.Parameters.AddWithValue("SevkEmriNo", dtsevkemri.Rows[i]["SevkEmriNo"]);
                        sqlCommand.Parameters.AddWithValue("BelgeTarih", dtsevkemri.Rows[i]["BelgeTarih"]);
                        sqlCommand.Parameters.AddWithValue("CariId", dtsevkemri.Rows[i]["CariId"]);
                        sqlCommand.Parameters.AddWithValue("CariKod", dtsevkemri.Rows[i]["CariKod"]);
                        sqlCommand.Parameters.AddWithValue("TeslimTarih", dtsevkemri.Rows[i]["TeslimTarih"]);
                        sqlCommand.Parameters.AddWithValue("Aciklama", dtsevkemri.Rows[i]["Aciklama"]);
                        sqlCommand.Parameters.AddWithValue("CariGrup", dtsevkemri.Rows[i]["CariGrup"]);
                        sqlCommand.Parameters.AddWithValue("KaynakProgram", 9);
                        if (sqlCommand.ExecuteNonQuery() != 1)
                        {
                            Utilities.Inf("Bilgiler yazılamadı!\n" + sqlCommand.CommandText);
                        }
                    }
                }

                odbcCommand.CommandText = "select " +
        "	d.semir_detayno as \"SevkEmriDetayId\", d.semir_masterno as \"SevkEmriId\", d.emir_no as \"SevkEmriNo\",  " +
        "	d.sip_masterno as \"SiparisId\", d.stok_kod as \"MalzemeKod\", cast(s.rowid as int) as \"MalzemeId\", " +
        "	cast(b.rowid as int) as \"BirimId\", s.birim as \"Birim\", cast(p.rowid as int) as \"DepoId\", p.depo_kod as \"DepoKod\", " +
        "	o.sip_no as \"OzelKod\", o.cari_kod as \"CariKod\", d.sira_no as \"SiraNo\",  " +
        "	d.ozellik_kod1 as \"OzelKod1\", d.sevk_miktar as \"Miktar\", d.dmiktar2 - d.teslim_miktar as \"Kalan\" " +
        "from pub.sevk_emirdet d, pub.stok_kart s, pub.birim b, pub.siparis_detay o, pub.depo p  " +
        "where d.firma_kod = ? and emir_durum = 1 " +
        "and d.firma_kod = s.firma_kod and d.stok_kod = s.stok_kod " +
        "and d.firma_kod = b.firma_kod and s.birim = b.birim " +
        "and d.firma_kod = o.firma_kod and d.sip_detayno = o.sip_detayno and d.sip_masterno = o.sip_masterno " +
        "and d.firma_kod = p.firma_kod and o.depo_kod = p.depo_kod and d.emir_no = ?";
                adapter = new OdbcDataAdapter(odbcCommand);
                DataTable dtsevkemridetay = new DataTable();
                adapter.Fill(dtsevkemridetay);
                if (dtsevkemridetay != null && dtsevkemridetay.Rows.Count > 0)
                {
                    sqlCommand.CommandText = "INSERT INTO dbo.SevkEmriDetaylari (SevkEmriDetayId,SevkEmriId,SevkEmriNo,SiparisId,MalzemeKod,MalzemeId,BirimId,Birim,DepoId,DepoKod,OzelKod,CariKod,SiraNo,OzelKod1,Miktar,Kalan,KaynakProgram) VALUES (@SevkEmriDetayId,@SevkEmriId,@SevkEmriNo,@SiparisId,@MalzemeKod,@MalzemeId,@BirimId,@Birim,@DepoId,@DepoKod,@OzelKod,@CariKod,@SiraNo,@OzelKod1,@Miktar,@Kalan,@KaynakProgram)";
                    for (int i = 0; i < dtsevkemridetay.Rows.Count; i++)
                    {
                        sqlCommand.Parameters.Clear();
                        sqlCommand.Parameters.AddWithValue("SevkEmriDetayId", dtsevkemridetay.Rows[i]["SevkEmriDetayId"]);
                        sqlCommand.Parameters.AddWithValue("SevkEmriId", dtsevkemridetay.Rows[i]["SevkEmriId"]);
                        sqlCommand.Parameters.AddWithValue("SevkEmriNo", dtsevkemridetay.Rows[i]["SevkEmriNo"]);
                        sqlCommand.Parameters.AddWithValue("SiparisId", dtsevkemridetay.Rows[i]["SiparisId"]);
                        sqlCommand.Parameters.AddWithValue("MalzemeKod", dtsevkemridetay.Rows[i]["MalzemeKod"]);
                        sqlCommand.Parameters.AddWithValue("MalzemeId", dtsevkemridetay.Rows[i]["MalzemeId"]);
                        sqlCommand.Parameters.AddWithValue("BirimId", dtsevkemridetay.Rows[i]["BirimId"]);
                        sqlCommand.Parameters.AddWithValue("Birim", dtsevkemridetay.Rows[i]["Birim"]);
                        sqlCommand.Parameters.AddWithValue("DepoId", dtsevkemridetay.Rows[i]["DepoId"]);
                        sqlCommand.Parameters.AddWithValue("DepoKod", dtsevkemridetay.Rows[i]["DepoKod"]);
                        sqlCommand.Parameters.AddWithValue("OzelKod", dtsevkemridetay.Rows[i]["OzelKod"]);
                        sqlCommand.Parameters.AddWithValue("CariKod", dtsevkemridetay.Rows[i]["CariKod"]);
                        sqlCommand.Parameters.AddWithValue("SiraNo", dtsevkemridetay.Rows[i]["SiraNo"]);
                        sqlCommand.Parameters.AddWithValue("OzelKod1", dtsevkemridetay.Rows[i]["OzelKod1"]);
                        sqlCommand.Parameters.AddWithValue("Miktar", dtsevkemridetay.Rows[i]["Miktar"]);
                        sqlCommand.Parameters.AddWithValue("Kalan", dtsevkemridetay.Rows[i]["Kalan"]);
                        sqlCommand.Parameters.AddWithValue("KaynakProgram", 9);
                        if (sqlCommand.ExecuteNonQuery() != 1)
                        {
                            Utilities.Inf("Bilgiler yazılamadı!\n" + sqlCommand.CommandText);
                        }
                    }
                }

                if (transaction != null)
                {
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                }
                Utilities.CloseAll();
                Utilities.Inf("Sevk emri bilgileri güncellendi.");
            }
            catch (SqlException sqlException)
            {
                Utilities.Msg("Bilgiler yazılırken hata oluştu:" + sqlException.Message);
            }
            catch (OdbcException odbcException)
            {
                Utilities.Msg("Bilgiler çekilirken hata oluştu:" + odbcException.Message);
            }
            catch (Exception exception)
            {
                Utilities.Msg("Bilgiler akatarılırken genel hata oluştu:" + exception.Message);
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    transaction = null;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            SqlTransaction transaction = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                lblEntegre.Text = "Bilgiler çekiliyor..."; Application.DoEvents();
                SqlCommand sqlCommand = Utilities.SqlConnection.CreateCommand();
                transaction = Utilities.SqlConnection.BeginTransaction();
                sqlCommand.Transaction = transaction;

                OdbcCommand odbcCommand = Utilities.OdbcConnection.CreateCommand();
                odbcCommand.CommandText = "select  " +
    "	e.semir_masterno as \"SevkEmriId\", e.emir_no as \"SevkEmriNo\", TO_CHAR(e.emir_tarih, 'yyyy-mm-dd') as \"BelgeTarih\", " +
    "	cast(c.rowid as int) as \"CariId\", e.cari_kod as \"CariKod\", " +
    "	TO_CHAR(e.yukleme_tarih, 'yyyy-mm-dd') as \"TeslimTarih\", '' as \"Aciklama\", " +
    "	c.cgrup_kod3 as \"CariGrup\", c.aktif_pasif as \"CariDurum\" " +
    "from pub.sevk_emir e, pub.cari_kart c " +
    "where e.firma_kod = ? and e.emir_durum = 1 " +
    "and e.firma_kod = c.firma_kod and e.cari_kod = c.cari_kod ";
                var param2 = new OdbcParameter("firma_kod", OdbcType.VarChar);
                param2.Value = Utilities.Config.UyumFirmaKod;
                odbcCommand.Parameters.Add(param2);

                OdbcDataAdapter adapter = new OdbcDataAdapter(odbcCommand);
                DataTable dtsevkemri = new DataTable();
                adapter.Fill(dtsevkemri);
                if (dtsevkemri != null && dtsevkemri.Rows.Count > 0)
                {
                    psevkemriguncelle.Value = 0; psevkemriguncelle.Maximum = dtsevkemri.Rows.Count;
                    lblEntegre.Text = "Bilgiler temizleniyor..."; Application.DoEvents();
                    sqlCommand.CommandText = "DELETE FROM dbo.SevkEmriDetaylari WHERE 1 = 1;DELETE dbo.SevkEmirleri WHERE 1 = 1;";
                    sqlCommand.Parameters.AddWithValue("SevkEmriNo", txtsevkemrino.Text);
                    sqlCommand.ExecuteNonQuery();

                    lblEntegre.Text = "Bilgiler yazılıyor..."; Application.DoEvents();
                    sqlCommand.CommandText = "INSERT INTO dbo.SevkEmirleri (SevkEmriId,SevkEmriNo,BelgeTarih,CariId,CariKod,TeslimTarih,Aciklama,CariGrup, KaynakProgram) VALUES (@SevkEmriId,@SevkEmriNo,@BelgeTarih,@CariId,@CariKod,@TeslimTarih,@Aciklama,@CariGrup, @KaynakProgram)";
                    for (int i = 0; i < dtsevkemri.Rows.Count; i++)
                    {
                        sqlCommand.Parameters.Clear();
                        sqlCommand.Parameters.AddWithValue("SevkEmriId", dtsevkemri.Rows[i]["SevkEmriId"]);
                        sqlCommand.Parameters.AddWithValue("SevkEmriNo", dtsevkemri.Rows[i]["SevkEmriNo"]);
                        sqlCommand.Parameters.AddWithValue("BelgeTarih", dtsevkemri.Rows[i]["BelgeTarih"]);
                        sqlCommand.Parameters.AddWithValue("CariId", dtsevkemri.Rows[i]["CariId"]);
                        sqlCommand.Parameters.AddWithValue("CariKod", dtsevkemri.Rows[i]["CariKod"]);
                        sqlCommand.Parameters.AddWithValue("TeslimTarih", dtsevkemri.Rows[i]["TeslimTarih"]);
                        sqlCommand.Parameters.AddWithValue("Aciklama", dtsevkemri.Rows[i]["Aciklama"]);
                        sqlCommand.Parameters.AddWithValue("CariGrup", dtsevkemri.Rows[i]["CariGrup"]);
                        sqlCommand.Parameters.AddWithValue("KaynakProgram", 9);
                        if (sqlCommand.ExecuteNonQuery() != 1)
                        {
                            Utilities.Inf("Bilgiler yazılamadı!\n" + sqlCommand.CommandText);
                        }
                        psevkemriguncelle.Value = i; Application.DoEvents();
                    }

                    lblEntegre.Text = "Detay bilgileri çekiliyor..."; Application.DoEvents();
                    odbcCommand.CommandText = "select 	d.semir_detayno as \"SevkEmriDetayId\", e.semir_masterno as \"SevkEmriId\", d.emir_no as \"SevkEmriNo\",  " +
"d.sip_masterno as \"SiparisId\", d.stok_kod as \"MalzemeKod\", cast(s.rowid as int) as \"MalzemeId\", 	" +
"cast(b.rowid as int) as \"BirimId\", s.birim as \"Birim\", cast(p.rowid as int) as \"DepoId\", 	" +
"p.depo_kod as \"DepoKod\",o.sip_no as \"OzelKod\", o.cari_kod as \"CariKod\", d.sira_no as \"SiraNo\",	" +
"d.ozellik_kod1 as \"OzelKod1\",d.sevk_miktar as \"Miktar\", d.dmiktar2 - d.teslim_miktar as \"Kalan\"	" +
"from pub.sevk_emirdet d, pub.stok_kart s, pub.birim b, pub.siparis_detay o, pub.depo p, pub.sevk_emir e	" +
"where d.firma_kod = ? and d.emir_durum = 1 and e.emir_durum = 1 and d.firma_kod = s.firma_kod and d.stok_kod = s.stok_kod	" +
"and d.firma_kod = b.firma_kod and s.birim = b.birim and d.firma_kod = o.firma_kod and d.semir_masterno = e.semir_masterno and d.firma_kod = e.firma_kod	" +
"and d.sip_detayno = o.sip_detayno and d.sip_masterno = o.sip_masterno and d.firma_kod = p.firma_kod and o.depo_kod = p.depo_kod  ";
                    adapter = new OdbcDataAdapter(odbcCommand);
                    DataTable dtsevkemridetay = new DataTable();
                    adapter.Fill(dtsevkemridetay);
                    if (dtsevkemridetay != null && dtsevkemridetay.Rows.Count > 0)
                    {
                        psevkemriguncelle.Value = 0; psevkemriguncelle.Maximum = dtsevkemridetay.Rows.Count;
                        lblEntegre.Text = "Bilgiler yazılıyor..."; Application.DoEvents();
                        sqlCommand.CommandText = "INSERT INTO dbo.SevkEmriDetaylari (SevkEmriDetayId,SevkEmriId,SevkEmriNo,SiparisId,MalzemeKod,MalzemeId,BirimId,Birim,DepoId,DepoKod,OzelKod,CariKod,SiraNo,OzelKod1,Miktar,Kalan,KaynakProgram) VALUES (@SevkEmriDetayId,@SevkEmriId,@SevkEmriNo,@SiparisId,@MalzemeKod,@MalzemeId,@BirimId,@Birim,@DepoId,@DepoKod,@OzelKod,@CariKod,@SiraNo,@OzelKod1,@Miktar,@Kalan,@KaynakProgram)";
                        for (int i = 0; i < dtsevkemridetay.Rows.Count; i++)
                        {
                            sqlCommand.Parameters.Clear();
                            sqlCommand.Parameters.AddWithValue("SevkEmriDetayId", dtsevkemridetay.Rows[i]["SevkEmriDetayId"]);
                            sqlCommand.Parameters.AddWithValue("SevkEmriId", dtsevkemridetay.Rows[i]["SevkEmriId"]);
                            sqlCommand.Parameters.AddWithValue("SevkEmriNo", dtsevkemridetay.Rows[i]["SevkEmriNo"]);
                            sqlCommand.Parameters.AddWithValue("SiparisId", dtsevkemridetay.Rows[i]["SiparisId"]);
                            sqlCommand.Parameters.AddWithValue("MalzemeKod", dtsevkemridetay.Rows[i]["MalzemeKod"]);
                            sqlCommand.Parameters.AddWithValue("MalzemeId", dtsevkemridetay.Rows[i]["MalzemeId"]);
                            sqlCommand.Parameters.AddWithValue("BirimId", dtsevkemridetay.Rows[i]["BirimId"]);
                            sqlCommand.Parameters.AddWithValue("Birim", dtsevkemridetay.Rows[i]["Birim"]);
                            sqlCommand.Parameters.AddWithValue("DepoId", dtsevkemridetay.Rows[i]["DepoId"]);
                            sqlCommand.Parameters.AddWithValue("DepoKod", dtsevkemridetay.Rows[i]["DepoKod"]);
                            sqlCommand.Parameters.AddWithValue("OzelKod", dtsevkemridetay.Rows[i]["OzelKod"]);
                            sqlCommand.Parameters.AddWithValue("CariKod", dtsevkemridetay.Rows[i]["CariKod"]);
                            sqlCommand.Parameters.AddWithValue("SiraNo", dtsevkemridetay.Rows[i]["SiraNo"]);
                            sqlCommand.Parameters.AddWithValue("OzelKod1", dtsevkemridetay.Rows[i]["OzelKod1"]);
                            sqlCommand.Parameters.AddWithValue("Miktar", dtsevkemridetay.Rows[i]["Miktar"]);
                            sqlCommand.Parameters.AddWithValue("Kalan", dtsevkemridetay.Rows[i]["Kalan"]);
                            sqlCommand.Parameters.AddWithValue("KaynakProgram", 9);
                            if (sqlCommand.ExecuteNonQuery() != 1)
                            {
                                Utilities.Inf("Bilgiler yazılamadı!\n" + sqlCommand.CommandText);
                            }
                            psevkemriguncelle.Value = i; Application.DoEvents();
                        }
                    }
                }

                if (transaction != null)
                {
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                }

                psevkemriguncelle.Value = 0; Application.DoEvents();
                Utilities.Inf("Sevk emri bilgileri güncellendi.");
            }
            catch (SqlException sqlException)
            {
                Utilities.Msg("Bilgiler yazılırken hata oluştu:" + sqlException.Message);
            }
            catch (OdbcException odbcException)
            {
                Utilities.Msg("Bilgiler çekilirken hata oluştu:" + odbcException.Message);
            }
            catch (Exception exception)
            {
                Utilities.Msg("Bilgiler akatarılırken genel hata oluştu:" + exception.Message);
            }
            finally
            {
                Utilities.CloseAll();
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    transaction = null;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void btncari_Click(object sender, EventArgs e)
        {
            SqlTransaction transaction = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                lblEntegre.Text = "Bilgiler çekiliyor..."; Application.DoEvents();
                SqlCommand sqlCommand = Utilities.SqlConnection.CreateCommand();
                transaction = Utilities.SqlConnection.BeginTransaction();
                sqlCommand.Transaction = transaction;

                OdbcCommand odbcCommand = Utilities.OdbcConnection.CreateCommand();
                odbcCommand.CommandTimeout = 999999999;
                odbcCommand.CommandText = "select cast(RowId as int) as \"CariId\",cari_kod as \"CariKod\",cari_ad as \"CariAd\",kisa_ad as \"KisaAd\", " +
                "bolge_kod as \"BolgeKod\",cgrup_kod3 as \"GrupKod\",adres1 as \"Adres1\",adres2 as \"Adres2\",adres3 as \"Adres3\", " +
                "fat_adres1 as \"FaturaAdres1\",fat_adres2 as \"FaturaAdres2\",fat_adres3 as \"FaturaAdres3\", " +
                "vergi_daire as \"VargiDaire\",vergi_no as \"VergiNo\",depo_kod as \"DepoKod\",cari_tip as \"CariTip\" " +
                "from pub.cari_kart where firma_kod = ? ";
                var param2 = new OdbcParameter("firma_kod", OdbcType.VarChar);
                param2.Value = Utilities.Config.UyumFirmaKod;
                odbcCommand.Parameters.Add(param2);

                OdbcDataAdapter adapter = new OdbcDataAdapter(odbcCommand);
                DataTable dtcari = new DataTable();
                adapter.Fill(dtcari);
                if (dtcari != null && dtcari.Rows.Count > 0)
                {
                    pcarientegre.Value = 0; pcarientegre.Maximum = dtcari.Rows.Count;
                    lblEntegre.Text = "Bilgiler temizleniyor..."; Application.DoEvents();
                    sqlCommand.CommandText = "DELETE FROM dbo.Cariler WHERE 1 = 1;";
                    sqlCommand.ExecuteNonQuery();

                    lblEntegre.Text = "Bilgiler yazılıyor..."; Application.DoEvents();
                    sqlCommand.CommandText = "INSERT INTO dbo.Cariler (CariId,CariKod,CariAd,KisaAd,BolgeKod,GrupKod,Adres1,Adres2,Adres3,FaturaAdres1,FaturaAdres2,FaturaAdres3,VergiDairesi,VergiNo,DepoKod,CariTip) VALUES (@CariId,@CariKod,@CariAd,@KisaAd,@BolgeKod,@GrupKod,@Adres1,@Adres2,@Adres3,@FaturaAdres1,@FaturaAdres2,@FaturaAdres3,@VergiDairesi,@VergiNo,@DepoKod,@CariTip)";
                    for (int i = 0; i < dtcari.Rows.Count; i++)
                    {
                        sqlCommand.Parameters.Clear();
                        sqlCommand.Parameters.AddWithValue("CariId", dtcari.Rows[i]["CariId"]);
                        sqlCommand.Parameters.AddWithValue("CariKod", dtcari.Rows[i]["CariKod"]);
                        sqlCommand.Parameters.AddWithValue("CariAd", dtcari.Rows[i]["CariAd"]);
                        sqlCommand.Parameters.AddWithValue("KisaAd", dtcari.Rows[i]["KisaAd"]);
                        sqlCommand.Parameters.AddWithValue("BolgeKod", dtcari.Rows[i]["BolgeKod"]);
                        sqlCommand.Parameters.AddWithValue("GrupKod", dtcari.Rows[i]["GrupKod"]);
                        sqlCommand.Parameters.AddWithValue("Adres1", dtcari.Rows[i]["Adres1"]);
                        sqlCommand.Parameters.AddWithValue("Adres2", dtcari.Rows[i]["Adres2"]);
                        sqlCommand.Parameters.AddWithValue("Adres3", dtcari.Rows[i]["Adres3"]);
                        sqlCommand.Parameters.AddWithValue("FaturaAdres1", dtcari.Rows[i]["FaturaAdres1"]);
                        sqlCommand.Parameters.AddWithValue("FaturaAdres2", dtcari.Rows[i]["FaturaAdres2"]);
                        sqlCommand.Parameters.AddWithValue("FaturaAdres3", dtcari.Rows[i]["FaturaAdres3"]);
                        sqlCommand.Parameters.AddWithValue("VergiDairesi", dtcari.Rows[i]["VargiDaire"]);
                        sqlCommand.Parameters.AddWithValue("VergiNo", dtcari.Rows[i]["VergiNo"]);
                        sqlCommand.Parameters.AddWithValue("DepoKod", dtcari.Rows[i]["DepoKod"]);
                        sqlCommand.Parameters.AddWithValue("CariTip", dtcari.Rows[i]["CariTip"]);
                        if (sqlCommand.ExecuteNonQuery() != 1)
                        {
                            Utilities.Inf("Bilgiler yazılamadı!\n" + sqlCommand.CommandText);
                        }
                        pcarientegre.Value = i; Application.DoEvents();
                    }

                }

                if (transaction != null)
                {
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                }

                pcarientegre.Value = 0; Application.DoEvents();
                Utilities.Inf("Bilgiler güncellendi.");
            }
            catch (SqlException sqlException)
            {
                Utilities.Msg("Bilgiler yazılırken hata oluştu:" + sqlException.Message);
            }
            catch (OdbcException odbcException)
            {
                Utilities.Msg("Bilgiler çekilirken hata oluştu:" + odbcException.Message);
            }
            catch (Exception exception)
            {
                Utilities.Msg("Bilgiler akatarılırken genel hata oluştu:" + exception.Message);
            }
            finally
            {
                Utilities.CloseAll();
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    transaction = null;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
        }

        private void DepoStokHataliKayitlar()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                lblEntegre.Text = "Bilgiler çekiliyor..."; Application.DoEvents();
                SqlCommand sqlCommand = Utilities.SqlConnection.CreateCommand();
                sqlCommand.CommandText = "SELECT MalzemeKod, Birim, RafKod, DepoKod, COUNT(*) AS KayıtSayisi, SUM(Miktar) AS Miktar, 0 AS UyumMiktar, 0 AS RafId FROM dbo.DepoStoklari WITH(NOLOCK) " +
                "GROUP BY MalzemeKod, Birim, RafKod, DepoKod " +
                "HAVING COUNT(*) > 1";
                SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                DataTable dthatali = new DataTable();
                adapter.Fill(dthatali);
                dataGridView1.DataSource = dthatali;
                if (dthatali != null && dthatali.Rows.Count > 0)
                {
                    OdbcCommand odbcCommand = Utilities.OdbcConnection.CreateCommand();
                    odbcCommand.CommandTimeout = 999999999;

                    for (int i = 0; i < dthatali.Rows.Count; i++)
                    {
                        odbcCommand.CommandText = "select top 5 cast(d.rowid as int) as \"depo_id\",ds.depo_kod, " +
"ds.stok_kod,ds.birim,ds.stok_miktar,cast(r.rowid as int) as \"raf_id\",r.raf_kod " +
"from pub.depo_stok ds inner join pub.depo d on " +
"ds.depo_kod = d.depo_kod and ds.firma_kod = d.firma_kod left outer join " +
"pub.raf r on r.firma_kod = ds.firma_kod and r.depo_kod = d.depo_kod " +
"where ds.firma_kod = ? and ds.depo_kod = ? and r.raf_kod = ? and ds.stok_kod = ?";
                        odbcCommand.Parameters.Clear();
                        var param1 = new OdbcParameter("firma_kod", OdbcType.VarChar);
                        param1.Value = Utilities.Config.UyumFirmaKod;
                        odbcCommand.Parameters.Add(param1);

                        var param2 = new OdbcParameter("depo_kod", OdbcType.VarChar);
                        param2.Value = dthatali.Rows[i]["DepoKod"];
                        odbcCommand.Parameters.Add(param2);

                        var param3 = new OdbcParameter("raf_kod", OdbcType.VarChar);
                        param3.Value = dthatali.Rows[i]["RafKod"];
                        odbcCommand.Parameters.Add(param3);

                        var param4 = new OdbcParameter("stok_kod", OdbcType.VarChar);
                        param4.Value = dthatali.Rows[i]["MalzemeKod"];
                        odbcCommand.Parameters.Add(param4);
                        using (OdbcDataReader reader = odbcCommand.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("stok_miktar")))
                                    dthatali.Rows[i]["UyumMiktar"] = reader.GetValue(reader.GetOrdinal("stok_miktar"));

                                if (!reader.IsDBNull(reader.GetOrdinal("raf_id")))
                                    dthatali.Rows[i]["RafId"] = reader.GetValue(reader.GetOrdinal("raf_id"));

                                reader.Close();
                            }
                        }
                    }
                }
            }
            catch (SqlException sqlException)
            {
                Utilities.Msg("Bilgiler yazılırken hata oluştu:" + sqlException.Message);
            }
            catch (OdbcException odbcException)
            {
                Utilities.Msg("Bilgiler çekilirken hata oluştu:" + odbcException.Message);
            }
            catch (Exception exception)
            {
                Utilities.Msg("Bilgiler akatarılırken genel hata oluştu:" + exception.Message);
            }
            finally
            {
                Utilities.CloseAll();
                Cursor.Current = Cursors.Default;
            }
        }

        private void yenileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DepoStokHataliKayitlar();
        }

        private void seçileniDüzeltToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlTransaction transaction = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                lblEntegre.Text = "Bilgiler çekiliyor..."; Application.DoEvents();
                SqlCommand sqlCommand = Utilities.SqlConnection.CreateCommand();
                transaction = Utilities.SqlConnection.BeginTransaction();
                sqlCommand.Transaction = transaction;

                if (dataGridView1.SelectedRows.Count > 0)
                {
                    var data = dataGridView1.SelectedRows[0].Cells;

                    sqlCommand.CommandText = "SELECT OID,Miktar FROM dbo.DepoStoklari WITH (NOLOCK) WHERE DepoKod = @DepoKod AND RafKod = @RafKod AND MalzemeKod = @MalzemeKod AND Raf <> @Raf";
                    sqlCommand.Parameters.Clear();
                    sqlCommand.Parameters.AddWithValue("DepoKod", data["DepoKod"].Value);
                    sqlCommand.Parameters.AddWithValue("RafKod", data["RafKod"].Value);
                    sqlCommand.Parameters.AddWithValue("MalzemeKod", data["MalzemeKod"].Value);
                    sqlCommand.Parameters.AddWithValue("Raf", data["RafId"].Value);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int id = Convert.ToInt32(reader.GetValue(reader.GetOrdinal("OID")));
                            double miktar = Convert.ToDouble(reader.GetValue(reader.GetOrdinal("Miktar")));
                            reader.Close();

                            sqlCommand.CommandText = "DELETE FROM dbo.DepoStoklari WHERE OID = @OIDSIL;UPDATE dbo.DepoStoklari SET Miktar = Miktar + @Miktar, Miktar2 = Miktar  WHERE DepoKod = @DepoKod AND RafKod = @RafKod AND MalzemeKod = @MalzemeKod AND Raf = @Raf";
                            sqlCommand.Parameters.Clear();
                            sqlCommand.Parameters.AddWithValue("DepoKod", data["DepoKod"].Value);
                            sqlCommand.Parameters.AddWithValue("RafKod", data["RafKod"].Value);
                            sqlCommand.Parameters.AddWithValue("MalzemeKod", data["MalzemeKod"].Value);
                            sqlCommand.Parameters.AddWithValue("Raf", data["RafId"].Value);
                            sqlCommand.Parameters.AddWithValue("Miktar", miktar);
                            sqlCommand.Parameters.AddWithValue("OIDSIL", id);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }

                }

                if (transaction != null)
                {
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                }

                Utilities.Inf("Depo stok bilgileri güncellendi.");
                DepoStokHataliKayitlar();
            }
            catch (SqlException sqlException)
            {
                Utilities.Msg("Bilgiler yazılırken hata oluştu:" + sqlException.Message);
            }
            catch (OdbcException odbcException)
            {
                Utilities.Msg("Bilgiler çekilirken hata oluştu:" + odbcException.Message);
            }
            catch (Exception exception)
            {
                Utilities.Msg("Bilgiler akatarılırken genel hata oluştu:" + exception.Message);
            }
            finally
            {
                Utilities.CloseAll();
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    transaction = null;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tümünüDüzeltToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SqlTransaction transaction = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                lblEntegre.Text = "Bilgiler çekiliyor..."; Application.DoEvents();
                SqlCommand sqlCommand = Utilities.SqlConnection.CreateCommand();
                transaction = Utilities.SqlConnection.BeginTransaction();
                sqlCommand.Transaction = transaction;

                if (dataGridView1.SelectedRows.Count > 0)
                {
                    for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                    {
                        var data = dataGridView1.SelectedRows[i].Cells;

                        sqlCommand.CommandText = "SELECT OID,Miktar FROM dbo.DepoStoklari WITH (NOLOCK) WHERE DepoKod = @DepoKod AND RafKod = @RafKod AND MalzemeKod = @MalzemeKod AND Raf <> @Raf";
                        sqlCommand.Parameters.Clear();
                        sqlCommand.Parameters.AddWithValue("DepoKod", data["DepoKod"].Value);
                        sqlCommand.Parameters.AddWithValue("RafKod", data["RafKod"].Value);
                        sqlCommand.Parameters.AddWithValue("MalzemeKod", data["MalzemeKod"].Value);
                        sqlCommand.Parameters.AddWithValue("Raf", data["RafId"].Value);

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                int id = Convert.ToInt32(reader.GetValue(reader.GetOrdinal("OID")));
                                double miktar = Convert.ToDouble(reader.GetValue(reader.GetOrdinal("Miktar")));
                                reader.Close();

                                sqlCommand.CommandText = "DELETE FROM dbo.DepoStoklari WHERE OID = @OIDSIL;UPDATE dbo.DepoStoklari SET Miktar = Miktar + @Miktar, Miktar2 = Miktar  WHERE DepoKod = @DepoKod AND RafKod = @RafKod AND MalzemeKod = @MalzemeKod AND Raf = @Raf";
                                sqlCommand.Parameters.Clear();
                                sqlCommand.Parameters.AddWithValue("DepoKod", data["DepoKod"].Value);
                                sqlCommand.Parameters.AddWithValue("RafKod", data["RafKod"].Value);
                                sqlCommand.Parameters.AddWithValue("MalzemeKod", data["MalzemeKod"].Value);
                                sqlCommand.Parameters.AddWithValue("Raf", data["RafId"].Value);
                                sqlCommand.Parameters.AddWithValue("Miktar", miktar);
                                sqlCommand.Parameters.AddWithValue("OIDSIL", id);
                                sqlCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }

                if (transaction != null)
                {
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null;
                }

                Utilities.Inf("Depo stok bilgileri güncellendi.");
                DepoStokHataliKayitlar();
            }
            catch (SqlException sqlException)
            {
                Utilities.Msg("Bilgiler yazılırken hata oluştu:" + sqlException.Message);
            }
            catch (OdbcException odbcException)
            {
                Utilities.Msg("Bilgiler çekilirken hata oluştu:" + odbcException.Message);
            }
            catch (Exception exception)
            {
                Utilities.Msg("Bilgiler akatarılırken genel hata oluştu:" + exception.Message);
            }
            finally
            {
                Utilities.CloseAll();
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction.Dispose();
                    transaction = null;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void dışarıAktarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0) return;
            SaveFileDialog sv = new SaveFileDialog();
            sv.Filter = "CSV|*.csv|Excel|*.xls";
            if (sv.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamWriter sw = new StreamWriter(sv.FileName, false, System.Text.Encoding.GetEncoding("windows-1254"));
                System.Data.DataTable dt = (System.Data.DataTable)dataGridView1.DataSource;
                sw.Write("<html charset=utf8:xmlns:o=\"urn:schemas-microsoft-com:office:office\"xmlns:x=\"urn:schemas-microsoft-com:office:excel\"<head><meta name=ProgId content=Excel.Sheet><meta name=Generator content=\"Microsoft Excel 9\"><!--[if gte mso 9]><xml> <x:ExcelWorkbook>  <x:ExcelWorksheets>   <x:ExcelWorksheet>    <x:Name>Sayfa1</x:Name>    <x:WorksheetOptions>     <x:Selected/>     <x:Panes>      <x:Pane>       <x:Number>3</x:Number>       <x:RangeSelection>A:A</x:RangeSelection>      </x:Pane>     </x:Panes>    </x:WorksheetOptions>   </x:ExcelWorksheet>     </x:ExcelWorksheets> </x:ExcelWorkbook></xml><![endif]--></head><body><table x:str>");

                sw.Write("<tr>");

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sw.Write("<td>" + dt.Columns[i].ColumnName + "</td>");
                }
                sw.Write("</tr>");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sw.Write("<tr>");
                    for (int j = 0; j < dt.Columns.Count; j++)
                        sw.Write("<td>" + dt.Rows[i][j].ToString() + "</td>");
                    sw.Write("</tr>");
                }

                sw.Write("</table></body></html>");
                sw.Close();
                Utilities.Inf("Dosya kaydedildi. " + sv.FileName);
            }
        }

    }
}
