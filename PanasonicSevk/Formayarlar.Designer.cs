﻿namespace PanasonicSevk
{
    partial class Formayarlar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtdriver = new System.Windows.Forms.TextBox();
            this.btnkapat = new System.Windows.Forms.Button();
            this.btnkaydet = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtfirmakod = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtdatabase = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Uyumsoft Bağlantı Ayarları";
            // 
            // txtdriver
            // 
            this.txtdriver.Location = new System.Drawing.Point(215, 43);
            this.txtdriver.Margin = new System.Windows.Forms.Padding(4);
            this.txtdriver.Name = "txtdriver";
            this.txtdriver.Size = new System.Drawing.Size(245, 22);
            this.txtdriver.TabIndex = 1;
            // 
            // btnkapat
            // 
            this.btnkapat.Location = new System.Drawing.Point(377, 111);
            this.btnkapat.Margin = new System.Windows.Forms.Padding(4);
            this.btnkapat.Name = "btnkapat";
            this.btnkapat.Size = new System.Drawing.Size(83, 31);
            this.btnkapat.TabIndex = 2;
            this.btnkapat.Text = "Kapat";
            this.btnkapat.UseVisualStyleBackColor = true;
            this.btnkapat.Click += new System.EventHandler(this.btnkapat_Click);
            // 
            // btnkaydet
            // 
            this.btnkaydet.Location = new System.Drawing.Point(286, 111);
            this.btnkaydet.Margin = new System.Windows.Forms.Padding(4);
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Size = new System.Drawing.Size(83, 31);
            this.btnkaydet.TabIndex = 2;
            this.btnkaydet.Text = "Kaydet";
            this.btnkaydet.UseVisualStyleBackColor = true;
            this.btnkaydet.Click += new System.EventHandler(this.btnkaydet_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Firma Kodu";
            // 
            // txtfirmakod
            // 
            this.txtfirmakod.Location = new System.Drawing.Point(215, 13);
            this.txtfirmakod.Margin = new System.Windows.Forms.Padding(4);
            this.txtfirmakod.Name = "txtfirmakod";
            this.txtfirmakod.Size = new System.Drawing.Size(245, 22);
            this.txtfirmakod.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 77);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Database Adı";
            // 
            // txtdatabase
            // 
            this.txtdatabase.Location = new System.Drawing.Point(215, 73);
            this.txtdatabase.Margin = new System.Windows.Forms.Padding(4);
            this.txtdatabase.Name = "txtdatabase";
            this.txtdatabase.Size = new System.Drawing.Size(245, 22);
            this.txtdatabase.TabIndex = 1;
            // 
            // Formayarlar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 155);
            this.Controls.Add(this.btnkaydet);
            this.Controls.Add(this.btnkapat);
            this.Controls.Add(this.txtfirmakod);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtdatabase);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtdriver);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Formayarlar";
            this.Text = "Ayarlar";
            this.Load += new System.EventHandler(this.Formayarlar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdriver;
        private System.Windows.Forms.Button btnkapat;
        private System.Windows.Forms.Button btnkaydet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtfirmakod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtdatabase;
    }
}